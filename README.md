##########################################################################
#  TRACY, A Tool for Accelerator Design and Analysis
##########################################################################

Self-Consistent Symplectic Integrator for charged particle beam dynamics.

The symplectic integrator for realistic modeling of magnetic lattices for
ring-based synchrotrons was initially implemented in Pascal, by the author,
with care taken for the software architecture and resulting records/modules
(-> "objects") to reflect the structure of the mathematical objects describing
the underlying beam dynamics model.


The symplectic integrator for RADIA kick maps was implemented by Laurent
Nadolski, SOLEIL, 2002.

The original Pascal library/code was machine translated to C (with p2c) by
Michael Boege, SLS, 1998.


The TRACY code was written by J. Bengtsson, É. Forest, and H.
Nishimura, then translated by M. Böge at SLS, extented by L. Nadolski at SOLEIL
(2002), revised by J. Bengtsson at BNL (2005). A compact user interface was added at
SOLEIL by J. Zhang (2011).

J. Bengtsson, “TRACY-2 User’s Manual”, SLS
J. Bengtsson, E. Forest, and H. Nishimura. Tracy3 User’s Manual.
Internal Document, February 1997; M. Böge,
“Update on TRACY-2 Documentation”, SLS Internal
Note, SLS-TME-TA-1999-0002, June 1999 

H. Nishimura. TRACY, A Tool for Accelerator Design and Analysis.
Proceedings of 1988 European Particle Accelerator Conference, pp. 803-805, Rome,
Italy, 1988


##########################################################################
## Numerical recipes
1. xutils-dev for makedepend
2. makedepend in the src file of nrecipes directory

##########################################################################

## Installation of Tracy3

What is necessary:
1. gcc
2. g++
3. autoconf
4. libtool
5. libgls0-dev lib + development library with header such as gls_sf.h

##########################################################################

Modify .bashrc to get environment variables 
cf set_var.sh or set_var4LNLS.sh
Set the path to fit your computer architecture

##########################################################################
**For compilation of TracyIII**
In root directory of TracyIII, run the command:
./make_for_gcc.sh opt 

An executable file is generated in directory TracyIII/tracy/tracy/bin:
soltracy

this file can be put in your path to run the code from anywhere

#########################################################################
Example to test the code: 
executable is soltracy located in  $TRACY_LIB/tools 

# TRACY HISTORY

The underlying beam dynamics model is open source; courtesy Michael Böge.
(A CORBA Based Distributed Client/Server Model for Beam Dynamics Applications at the SLS ICALEPS'99)

J. Bengtsson implemented and coded Tracy-2 as a Pascal beam dynamics library in 1990 (having been a TA for ditto at the dept. of software engineering, Lund Inst. of Tech.); based on a 4th order symplectic integrator. While Michael's machine translation mangled the (Pascal data) record structure quite badly – my implementation was object oriented [implicitly; since Pascal does not support objects] – the software architecture enabled him to pull it off.

Johan was tasked with providing an on-line model to guide the ALS commissioning. I.e., contrarily, Tracy was a 2x2 matrix code by Hiroshi Nishimura also coded in Pascal (TRACY, A Tool for Accelerator Design and Analysis EPAC'88); which was found to be inadequate for an on-line model (assuming e.g. mid-plane symmetry -> no linear coupling, etc.)

Johan used it to guide the control of the nonlinear dynamics for the SLS conceptual design in the mid-1990s.
(Having validated the model by beam studies for the ALS commissioning (& also provided a realistic estimate of the Touschek lifetime; for a medium energy ring): Modeling of Beam Dynamics and Comparison with Measurements for the Advanced Light Source (ALS) EPAC'94)

After which M. Böge machine translated my beam dynamics library from Pascal to C (with GNU p2c) & used it as an on-line model server to guide the commissioning (i.e., including e.g. modeling of girder correlations, etc. & the related correction algorithms that I provided to model the real lattice for the SLS conceptual design).
