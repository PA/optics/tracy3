#!/bin/bash

#===================================================
# script to compile non-parallel version tracy
#===================================================


# not sh since on Ubuntu sh is not bash
set -x # for debugging

if [ $# -ne 1 ] 
then                    # affiche l'usage
  echo "Missing parameter" 
  echo "Use: $0 debug for debug" 
  echo "Use: $0 opt for optimization result" 
  echo "Use: $0 mpi for optimization result" 
  exit 1
fi



dir=`pwd`
#NUM_REC="$dir/num_rec"

#INSTALL="/usr/bin/install"

TRACY="tracy"

cd "$dir/$TRACY"

# CVS file is inside, do not erase
rm -f autom4te.cache/*
rm -rf aclocal.m4

make distclean

# Clean everything and call autoreconf
sh bootstrap.sh

if [ $1 == "debug" ]
then 
  ./configure --prefix=$dir/$TRACY/tracy FFLAGS="-g -O2  -Wall -fbounds-check" CFLAGS="-g -O2  -Wall -fno-implicit-templates" CXXFLAGS="-g -O2  -Wall -fno-implicit-templates"
fi

if [ $1 == "opt" ]
then 
./configure --prefix=$dir/$TRACY/tracy FFLAGS="-O2  -Wall -fbounds-check" CFLAGS="-O2  -Wall -fno-implicit-templates" CXXFLAGS="-O2  -Wall -fno-implicit-templates"
fi

if [ $1 == "mpi" ]
then 
./configure --enable-mpi-exec --prefix=$dir/$TRACY/tracy  FFLAGS="-O2  -Wall -fbounds-check" CFLAGS="-O2  -Wall -fno-implicit-templates" CXXFLAGS="-O2  -Wall -fno-implicit-templates"
fi

#make
#make install
