# set environment variable for running TracyIII
# 32 bits MAC OS X
#export LIBPATH=/usr/lib/gcc/i686-apple-darwin10/4.2.1
# 64 bit MAC OS X
#export LIBPATH=/usr/lib/gcc/i686-apple-darwin10/4.2.1/x86_64/
export LD_LIBRARY_PATH=$LIBPATH:/usr/local/lib:/usr/lib:/usr/local/include
export CODEDIR=$HOME/Documents/Travail/codes
export NUM_REC=$CODEDIR/nrecipes/recipes_c-ansi
export TRACY_LIB=$CODEDIR/tracy/TracyIII/tracy
export THOR_LIB=$HOME/Thor-2.0
export LAT="$HOME/projects/in/lattice"

