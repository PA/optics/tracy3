ps = 0; eps = 0; CM = 0;

if (!ps) set terminal x11;
if (ps && !eps) set terminal postscript enhanced color solid;
if (ps && eps) set terminal postscript eps enhanced color solid;

set grid;

if (ps) set output "Touschek.ps"
set title "Momentum Aperture";
set xlabel "s [m]";
set ylabel "{/Symbol d} [%]";
if (!CM) \
  plot "Touschek.out" using 2:3 notitle with fsteps 2, \
       "Touschek.out" using 2:4 notitle with fsteps 2;
if (CM) \
  set yrange [0:]; \
  plot "momentum_acceptance_p.dat" using 1:2 notitle with fsteps 2, \
       "momentum_acceptance_n.dat" using 1:2 notitle with fsteps 2;
if (!ps) pause -1;
