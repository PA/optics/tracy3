ps = 0; eps = 0; action_angle = 0; pert = 0;

#N = 1; N_x = 33; N_y = 16;
N = 3; N_x = 11; N_y = 5;
# APS
#N = 40; N_x = 0; N_y = 0;
#N = 15; N_x = 2; N_y = 1;
#N = 3; N_x = 6; N_y = 2;
# MAX-IV
#N = 20; N_x = 2; N_y = 0;

font_size = 30; line_width = 2;
if (!ps) set terminal x11;
if (ps && !eps) \
  set terminal postscript enhanced color solid \
  lw line_width "Times-Roman" font_size;
if (ps && eps) \
  set terminal postscript eps enhanced color solid \
  lw line_width "Times-Roman" font_size;

#set multiplot;

# left adjusted labels
set key Left;

set grid;

set style line 1 lt 1 lw line_width lc rgb "blue";
set style line 2 lt 1 lw line_width lc rgb "dark-green";
set style line 3 lt 1 lw line_width lc rgb "red";
set style line 4 lt 1 lw line_width lc rgb "dark-orange";

set clabel "%5.2f"; set key left;

set palette rgbformulae 22, 13, -31 negative;

if (ps) set output "dnu_1.ps"
#set size 0.5, 0.5; set origin 0.0, 0.5;
set title "{/Symbol n}_x vs. A_{x,y}";
if (!action_angle) \
  set xlabel "A_{x,y} [mm]"; set ylabel "{/Symbol n}_x"; \
  if (!pert) \
    plot "dnu_dAx.out" using 1:(N*(N_x+$5)) title "A_x" \
          with linespoints ls 1, \
         "dnu_dAy.out" using 2:(N*(N_x+$5)) title "A_y" with linespoints ls 3;
  if (pert) \
    plot "dnu_dAx.out" using 1:(N*(N_x+$5)) title "A_x" \
          with linespoints ls 1, \
         "dnu_dAy.out" using 2:(N*(N_x+$5)) title "A_y" \
         with linespoints ls 3, \
         "dnu_dAx_pert.out" using 1:(N*(N_x+$3)) title "A_x (pert)" \
         with linespoints ls 2, \
         "dnu_dAy_pert.out" using 2:(N*(N_x+$3)) title "A_y (pert)" \
         with linespoints ls 4;
if (action_angle) \
  set xlabel "A_{x,y} [mm]"; set ylabel "{/Symbol n}_x"; \
  set ytics nomirror; set y2tics; \
  plot "dnu_dAx.out" using 3:5 title "J_x" with linespoints ls 1, \
       "dnu_dAy.out" using 4:5 title "J_y" with linespoints ls 3;
if (!ps) pause -1;

if (ps) set output "dnu_2.ps"
#set size 0.5, 0.5; set origin 0.5, 0.5;
set title "{/Symbol n}_y vs. A_{x,y}";
if (!action_angle) \
  set xlabel "A_{x,y} [mm]"; set ylabel "{/Symbol n}_y"; \
  if (!pert) \
    plot "dnu_dAx.out" using 1:(N*(N_y+$6)) title "A_x" \
          with linespoints ls 1, \
         "dnu_dAy.out" using 2:(N*(N_y+$6)) title "A_y" \
         with linespoints ls 3;
  if (pert) \
    plot "dnu_dAx.out" using 1:(N*(N_y+$6)) title "A_x" \
         with linespoints ls 1, \
         "dnu_dAy.out" using 2:(N*(N_y+$6)) title "A_y" \
         with linespoints ls 3, \
         "dnu_dAx_pert.out" using 1:(N*(N_y+$4)) title "A_x (pert)" \
         with linespoints ls 2, \
         "dnu_dAy_pert.out" using 2:(N*(N_y+$4)) title "A_y (pert)" \
         with linespoints ls 4;
if (action_angle) \
  set xlabel "A_{x,y} [mm]"; set ylabel "{/Symbol n}_y"; \
  plot "dnu_dAx.out" using 3:6 title "J_x" with linespoints ls 1, \
       "dnu_dAy.out" using 4:6 title "J_y" with linespoints ls 3;
if (!ps) pause -1;

if (ps) set output "dnu_3.ps"
#set size 0.5, 0.5; set origin 0.0, 0.0;
set title "Chromaticity";
set xlabel "{/Symbol d} [%]"; set ylabel "{/Symbol n}_x";
set y2label "{/Symbol n}_y";
set ytics nomirror; set y2tics;
if (!pert) \
  plot "chrom2.out" using 1:(N*$2) title "{/Symbol n}_x" with lines ls 1, \
       "chrom2.out" using 1:(N*$3) axis x1y2 title "{/Symbol n}_y" \
       with lines ls 3;
if (pert) \
  plot "chrom2.out" using 1:(N*$2) title "{/Symbol n}_x" with lines ls 1, \
       "chrom2.out" using 1:(N*$3) axis x1y2 title "{/Symbol n}_y" \
       with lines ls 3, \
       "chrom2_pert.out" using 1:(N*(N_x+$2)) axis x1y1 \
       title "{/Symbol n}_x (pert)" with lines ls 2, \
       "chrom2_pert.out" using 1:(N*(N_y+$3)) axis x1y2 \
       title "{/Symbol n}_y (pert)" with lines ls 4;
if (!ps) pause -1;

if (ps) set output "dnu_4.ps"
#set size 0.5, 0.5; set origin 0.0, 0.0;
set title "Rec. Chrom.: Quadradic Deviation";
set xlabel "{/Symbol d} [%]"; set ylabel "{/Symbol n}_x";
set y2label "{/Symbol n}_y";
set ytics nomirror; set y2tics;
plot "chrom2.out" using 1:((N*$2-33.134)**2) title "{/Symbol n}_x" with lines ls 1, \
     "chrom2.out" using 1:((N*$3-16.180)**2) axis x1y2 title "{/Symbol n}_y" \
     with lines ls 3;
if (!ps) pause -1;

fract(x) = x - int(x);

#exit();

set noztics; unset colorbox; set key left;
set view 0, 0, 1, 1;

if (ps) set output "dnu_5.ps"
set title "Distance from \"3rd\" Order Resonances";
set xlabel "{/Symbol d} [%]"; set ylabel "{/Symbol Dn}"; unset y2label;
set ytics nomirror; unset y2tics;
set yrange [0:1];
splot "chrom2.out" using 1:(fract(N*($2))):(0.0) \
      title "{/Symbol n}_x" with lines palette z, \
      "chrom2.out" using 1:(fract(N*($3))):(0.1) \
      title "{/Symbol n}_y" with lines palette z, \
      "chrom2.out" using 1:(fract(N*2*($2))):(0.2) \
      title "2{/Symbol n}_x" with lines palette z, \
      "chrom2.out" using 1:(fract(N*2*($3))):(0.3) \
      title "2{/Symbol n}_y" with lines palette z, \
      "chrom2.out" using 1:(fract(N*(($2)+($3)))):(0.6) \
      title "{/Symbol n}_x+{/Symbol n}_y" with lines palette z, \
      "chrom2.out" using 1:(fract(N*(($2)-($3)))):(0.7) \
      title "{/Symbol n}_x-{/Symbol n}_y" with lines palette z, \
      "chrom2.out" using 1:(fract(N*3*($2))):(0.8) \
      title "3{/Symbol n}_x" with lines palette z, \
      "chrom2.out" using 1:(fract(N*(($2)+2*($3)))):(0.9) \
      title "{/Symbol n}_x+2{/Symbol n}_y" with lines palette z, \
      "chrom2.out" using 1:(fract(N*(($2)-2*($3)))):(1.0) \
      title "{/Symbol n}_x-2{/Symbol n}_y" with lines palette z;
if (!ps) pause -1;

if (ps) set output "dnu_6.ps"
set title "Distance from \"4th\" Order Sextupolar Resonances";
set xlabel "{/Symbol d} [%]"; set ylabel "{/Symbol Dn}"; unset y2label;
set yrange [0:1];
set ytics nomirror; unset y2tics;
splot "chrom2.out" using 1:(fract(N*4*($2))):(0.0) \
      title "4{/Symbol n}_x" with lines palette z, \
      "chrom2.out" using 1:(fract(N*4*($3))):(0.2) \
      title "4{/Symbol n}_y" with lines palette z, \
      "chrom2.out" using 1:(fract(N*(2*($2)+2*($3)))):(0.8) \
      title "2{/Symbol n}_x+2{/Symbol n}_y" with lines palette z, \
      "chrom2.out" using 1:(fract(N*(2*($2)-2*($3)))):(1.0) \
      title "2{/Symbol n}_x-2{/Symbol n}_y" with lines palette z;
if (!ps) pause -1;

if (ps) set output "dnu_7.ps"
set title "Distance from Resonances \"5th\" Order Sextupolar Resonances";
set xlabel "{/Symbol d} [%]"; set ylabel "{/Symbol Dn}";
set ytics nomirror; unset y2tics;
splot "chrom2.out" using 1:(fract(N*5*($2))):(0.0) \
      title "5{/Symbol n}_x" with lines palette z, \
      "chrom2.out" using 1:(fract(N*5*($3))):(0.1) \
      title "5{/Symbol n}_y" with lines palette z, \
      "chrom2.out" using 1:(fract(N*(($2)+4*($3)))):(0.2) \
      title "{/Symbol n}_x+4{/Symbol n}_y" with lines palette z, \
      "chrom2.out" using 1:(fract(N*(($2)-4*($3)))):(0.8) \
      title "{/Symbol n}_x-4{/Symbol n}_y" with lines palette z, \
      "chrom2.out" using 1:(fract(N*(3*($2)+2*($3)))):(0.9) \
      title "3{/Symbol n}_x+2{/Symbol n}_y" with lines palette z, \
      "chrom2.out" using 1:(fract(N*(3*($2)-2*($3)))):(1.0) \
      title "3{/Symbol n}_x-2{/Symbol n}_y" with lines palette z;
if (!ps) pause -1;
