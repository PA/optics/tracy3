ps = 0; eps = 1; contour = 1;

if (!ps) set terminal x11;
if (ps && eps) set terminal postscript eps enhanced color solid;
if (ps  && !eps) set terminal postscript enhanced color solid;

if (contour) set nosurface;
set hidden3d;
#set contour base;
set grid;

# rainbow
#set palette rgbformulae 33, 13, 10;
set palette rgbformulae 22, 13, -31;
#set palette defined (0 "blue", 3 "green", 6 "yellow", 10 "red");
set pm3d at s;
#set pm3d at b map;

#set cntrparam level 10;
set cntrparam levels incremental -2, -1, -8;

# x <-> horizontal, y <-> vertical, z <-> perpendicular to screen
# rot_x, rot_z, scale, scale_z
if (!contour) set view 65, 15, 1, 1;
if (contour) set view 0, 0, 1, 1;


if (ps) set output "fmap_1.ps"

set title "Frequency Map;
set xlabel "{/Symbol n}_x"; set ylabel "{/Symbol n}_y";

#splot "fmap.out" using 3:4:7 notitle with lines lt palette z;
#if (!ps) pause(-1);


if (ps) set output "fmap_2.ps"
set xlabel "A_x"; set ylabel "A_y";
set zrange[:-2.0];

splot "fmap.out" using 1:2:7 notitle lt palette z;
if (!ps) pause(-1);
