ps = 0; eps = 1;

if (!ps) set terminal x11;
if (ps && !eps) set terminal postscript enhanced color solid;
if (ps && eps) set terminal postscript eps enhanced color solid;

set grid;

if (ps) set output "linlat_1.ps"
set title "Beta Functions";
set xlabel "s [m]";
set ylabel "[m]";
set y2range [-1.5:20];
plot "linlat1.dat" using 1:6 title "{/Symbol b}_x" with lines 1, \
     "linlat1.dat" using 1:11 title "{/Symbol b}_y" with lines 2;
if (!ps) pause -1;

if (ps) set output "linlat_2.ps"
set title "Dispersion";
set xlabel "s [m]";
set ylabel "Eta [m]";
set y2range [-1.5:20];
plot "linlat1.dat" using 1:8 title "{/Symbol h}_x" with lines 1, \
     "linlat1.dat" using 1:13 title "{/Symbol h}_y" with lines 2;
if (!ps) pause -1;
