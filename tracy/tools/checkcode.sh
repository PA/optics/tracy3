#!/bin/bash
#set -x # for debugging

if [ $# -ne 1 ] 
then                    # affiche l'usage
    echo "Missing parameter" 
    echo "Use: $0 diff for comparing result" 
    echo "Use: $0 compute for computing result" 
    echo "Use: $0 overwrite for writing _new files to _ref files" 
    exit 1
fi

if [ $1 == "help" ] || [ $1 == "--help" ] 
then 
    echo "Use: $0 diff for comparing result" 
    echo "Use: $0 compute for computing result" 
    exit 1
fi

if [ $1 != "help" ] && [ $1 != "--help" ] && [ $1 != "compute" ] &&
 [ $1 != "diff" ] && [ $1 != "overwrite" ]
then 
    echo "Wrong Parameter"
    echo "Use: $0 diff for comparing result" 
    echo "Use: $0 compute for computing result" 
    exit 1
fi

# compute and save result in _new
if [ $1 == "compute" ]
then 
soltracy Input_checkcode.prm

CMD='mv'

$CMD nudx.out nudx_new.out
$CMD nudz.out nudz_new.out
$CMD nudp.out nudp_new.out
$CMD flat_file_error.dat flat_file_error_new.dat
$CMD linlat.out linlat_new.out
$CMD chambre.out chambre_new.out
$CMD flat_file.dat flat_file_new.dat
fi

if [ $1 == "diff" ]
then 

CMD='diff'
$CMD nudx_ref.out nudx_new.out
$CMD nudz_ref.out nudz_new.out
$CMD nudp_ref.out nudp_new.out
$CMD flat_file_error_ref.dat flat_file_error_new.dat
$CMD linlat_ref.out linlat_new.out
$CMD chambre_ref.out chambre_new.out
$CMD flat_file_ref.dat flat_file_new.dat
fi

# overwrite reference file with new files
if [ $1 == "overwrite" ]
then 

echo "$@" | fmt 
echo -n "$0: execute that command line? [yN](N) "
read ans 
case "$ans" in
    [yY]*) 
    CMD='mv'
echo "overwrite reference files"
$CMD nudx_new.out nudx_ref.out
$CMD nudz_new.out nudz_ref.out
$CMD nudp_new.out nudp_ref.out
$CMD flat_file_error_new.dat flat_file_error_ref.dat
$CMD linlat_new.out linlat_ref.out
$CMD chambre_new.out chambre_ref.out
$CMD flat_file_new.dat flat_file_ref.dat
;;
    *|"") echo "$0: NOT executing: $*" | fmt ;;
esac

fi 

