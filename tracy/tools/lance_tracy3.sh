#!/bin/sh
# 30 Septembre 2010, Laurent S. nadolski, Synchrotron SOLEIL
#Option pour voir les details
#set -x

if [ $# -ne 1 ]         # si le nombre de parametres n'est pas 1
then                    # affiche l'usage
    echo Tracy3 not valid
    echo Missing paremeter file
    echo Use: $0 parameterfile 
    exit 1
fi

## creation d'un nom unique pour l'executable
NUM=`date '+%d%m%y%H%M%s'`
DIR=$HOME/resultat_tracy/exe
NOM=$DIR/soltracy_$NUM
# take file name and remove .prm extension if exists
INPUT_FILE=$(basename $1 .prm).prm
SCRIPT_FILE=$DIR/$(basename $1 .prm)_$NUM
echo excecutable filename: $NOM
echo input parameter filename: $INPUT_FILE

# generate name for excutable
cp soltracy $NOM
# generate name for input parameters
cp  $INPUT_FILE $SCRIPT_FILE.prm

## creation d'un nom unique pour le job PBS
JOB=$DIR/job.$NUM

#########################
## creation du script PBS
echo "#PBS -l walltime=72:00:00,mem=400mb,nodes=1:ppn=1
#PBS -N Tracy
#PBS -m abe
#PBS -j eo
# IMPORTANT : mettre ici le repertoire ou l'on execute tracy
#             le chemin doit etre absolu
set -x

### va dans /tmpdir/PBS du noeud ou le job est excecute
cd \$TMPDIR

### Excecute le job : ici soltracy
$NOM $SCRIPT_FILE

### Sauvegarde des resultats *.out
uname -a
pwd
ls -l
DIRSAVE=$HOME/resultat_tracy
mkdir \$DIRSAVE/result.\$PBS_JOBID
mv *.out \$DIRSAVE/result.\$PBS_JOBID
mv *.dat \$DIRSAVE/result.\$PBS_JOBID

rm $NOM
mv $SCRIPT_FILE.prm \$DIRSAVE/result.\$PBS_JOBID/$INPUT_FILE

### email pour dire ou sont les resultats du job
CMD=\"mail -s \\\"Job \$PBS_JOBID\\\" \"`cat $HOME/.forward`
ssh isei \"\$CMD\" <<EOF
Bonjour,

Le demon du Cluster vous informe que votre job est termine:
Les resultats sont dans \$DIRSAVE/result.\$PBS_JOBID

A bientot,

Le Cluster.
EOF
" > $JOB
## creation du script PBS
#########################

### lance la commande PBS
#qsub -q redhat9 -l nodes=1:ppn=1:redhat9 $JOB
qsub $JOB

### fait le menage
rm $JOB
