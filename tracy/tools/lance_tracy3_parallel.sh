#!/bin/sh
# 04 February 2012, Laurent S. Nadolski, Synchrotron SOLEIL
#Option pour voir les details
#set -x

if [ $# -ne 1 ]         # si le nombre de parametres n'est pas 1
then                    # affiche l'usage
    echo Tracy3 not valid
    echo Missing paremeter file
    echo Use: $0 parameterfile 
    exit 1
fi

## create unique name for executable file
NUM=`date '+%d%m%y%H%M%s'`
DIR=$HOME/resultat_tracy/exe

# take file name and remove .prm extension if exists
INPUT_DIR=$PWD
INPUT_FILE=$(basename $1 .prm).prm
EXEC_DIR=/usr/local/applications/physmach/tracy3
NOM=$EXEC_DIR/psoltracy3

echo executable filename: $NOM
echo input parameter filename: $INPUT_FILE

JOB=psoltracy_script.$NUM

#########################
## creation du script PBS
echo "#!/bin/bash
#PBS -S /bin/bash
#PBS -N ParallelTracy
#PBS -j eo
#PBS -l nodes=2:ppn=8
#PBS -l mem=2000mb
#PBS -l walltime=1000:00:00


if [[ \$PBS_ENVIRONMENT = \"PBS_BATCH\" ]]; then 
 echo PBS_BATCH
 N=\`cat \$PBS_NODEFILE |wc -l\`
 echo \" ============================\"
 echo \"Execution on:\"
 cat \$PBS_NODEFILE
 echo \" ============================\"
else 
 echo \"Not a batch job !\"
 exit
fi

# IMPORTANT : put here the absolute name of the executable 
set -x

## execute command

echo executable filename: $NOM
echo input parameter filename: $INPUT_DIR/$INPUT_FILE

### va dans /tmpdir/PBS du noeud ou le job est execute
cd \$TMPDIR
cp $INPUT_DIR/$INPUT_FILE  .

### Excecute job soltracy parallel version
ulimit -s unlimited
time mpirun -n \$N -f \$PBS_NODEFILE -r ssh $NOM $INPUT_FILE

### SAVE THE RESULTS
DIRSAVE=\$HOME/resultat_tracy
mkdir -p \$DIRSAVE/result.\$PBS_JOBID
mv *.prm \$DIRSAVE/result.\$PBS_JOBID
mv [^0-9]*.out \$DIRSAVE/result.\$PBS_JOBID  #not save the individual file
mv [^0-9]*.dat \$DIRSAVE/result.\$PBS_JOBID  #not save the individual file


### email pour dire ou sont les resultats du job
CMD=\"mail -s \\\"Job \$PBS_JOBID\\\" \"\`cat $HOME/.forward\`
ssh isei \"\$CMD\" <<EOF
Hello $USER,

Work is done!

Please check results in \$DIRSAVE/result.\$PBS_JOBID

Hoping to see you again in our computation universe

Your friend ISEI
EOF
" > $JOB

## creation du script PBS
#########################

### launch PBS command
qsub -v INPUT_FILE=$1  $JOB

### clean up
rm $JOB 
