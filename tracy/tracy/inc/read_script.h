/* class to read the parameters of the bool flags in the user input script */
 class UserCommand
 {
   public:
   char CommandStr[max_str];   // bool flag name
   
 /***** parameters *******/
  
    //RFVoltFlag
   double RFvolt;   // RF voltage
   
   //chamber file
   char chamber_file[max_str];
   
    // misalignment error file
   char ae_file[max_str];
   
   // field error file
   char fe_file[max_str]; 
   
    // FitTuneFlag ; 
   char qf[max_str],qd[max_str]; 
   double targetnux, targetnuz;
   // FitTune4Flag  ; 
	 char qf1[max_str],qf2[max_str],qd1[max_str],qd2[max_str]; 
   // FitChromFlag ; 
   char sxm1[max_str],sxm2[max_str]; 
   double targetksix, targetksiz ;
   //add coupling error to full/half quadrupoles
   long err_seed; 
   double err_rms;
   
	//start track particle coordinates; PrintTrackFlag
   char _PrintTrack_track_file[max_str];
   double _PrintTrack_x, _PrintTrack_px, _PrintTrack_y, 
          _PrintTrack_py, _PrintTrack_delta, _PrintTrack_ctau;
   long  _PrintTrack_nmax; 
   
   //twiss file 
   char _PrintTwiss_twiss_file[max_str];
   
   char _PrintCOD_cod_file[max_str];
  
  //AmplitudeTuneShiftFlag;
   char   _AmplitudeTuneShift_nudx_file[max_str];
   char   _AmplitudeTuneShift_nudz_file[max_str];
   long   _AmplitudeTuneShift_nxpoint, _AmplitudeTuneShift_nypoint;
   long   _AmplitudeTuneShift_nturn;
   double _AmplitudeTuneShift_xmax, _AmplitudeTuneShift_ymax, _AmplitudeTuneShift_delta;
 
 //EnergyTuneShiftFlag;
   char   _EnergyTuneShift_nudp_file[max_str];
   long   _EnergyTuneShift_npoint, _EnergyTuneShift_nturn;
   double _EnergyTuneShift_deltamax;
 
 //extern bool FmapFlag;
   char   _FmapFlag_fmap_file[max_str];
   long   _FmapFlag_nxpoint, _FmapFlag_nypoint, _FmapFlag_nturn;
   double _FmapFlag_xmax, _FmapFlag_ymax, _FmapFlag_delta;
   bool   _FmapFlag_diffusion;
   bool   _FmapFlag_printloss;
   
 
 //extern bool FmapdpFlag;
   char   _FmapdpFlag_fmapdp_file[max_str];
   long   _FmapdpFlag_nxpoint, _FmapdpFlag_nepoint, _FmapdpFlag_nturn;
   double _FmapdpFlag_xmax, _FmapdpFlag_emax, _FmapdpFlag_z;
   bool   _FmapdpFlag_diffusion;
   bool   _FmapdpFlag_printloss;

   
  //MomentumAccFlag;
   char   _MomentumAccFlag_momacc_file[max_str];  //  file to save tracked momentum accpetance
   char   _MomentumAccFlag_TrackDim[3];
   long   _MomentumAccFlag_istart, _MomentumAccFlag_istop,
          _MomentumAccFlag_nstepn, _MomentumAccFlag_nstepp,
          _MomentumAccFlag_nturn;
   double _MomentumAccFlag_deltaminn, _MomentumAccFlag_deltamaxn;
   double _MomentumAccFlag_deltaminp, _MomentumAccFlag_deltamaxp;
   double _MomentumAccFlag_zmax;
   
  // /* Phase space */
   //char *_Phase_phase_file;
   char _Phase_phase_file[max_str];
   char _Phase_Dim[3];
   double _Phase_X, _Phase_Px, _Phase_Y, _Phase_Py,_Phase_delta, _Phase_ctau;
   long _Phase_nturn;
   bool _Phase_Damping;
 
   //Touschek lifetime
   bool TouschekFlag, IBSFlag, TousTrackFlag;
   
   // ID correction
   bool IDCorrFlag;
     
  //set default values
  UserCommand(void)   //constructor
 {  
   /* start tracking coordinates */
    strcpy(_PrintTrack_track_file,"track.out");
    _PrintTrack_x = 0.001, _PrintTrack_px=0.0, _PrintTrack_y=0.0, 
    _PrintTrack_py=0.0, _PrintTrack_delta=0.0, _PrintTrack_ctau=0.0;
    _PrintTrack_nmax=50L; 
   
    /* twiss file */
    strcpy(_PrintTwiss_twiss_file,"twiss.out");
    
    /*COD file*/
    strcpy(_PrintCOD_cod_file,"printcod.out");
  
  // /* fmap for on momentum particle*/
   strcpy(_FmapFlag_fmap_file,"fmap.out");
   _FmapFlag_nxpoint=31L, _FmapFlag_nypoint=21L, _FmapFlag_nturn=516L;
   _FmapFlag_xmax=0.025, _FmapFlag_ymax=0.005, _FmapFlag_delta=0.0;
   _FmapFlag_diffusion = true, _FmapFlag_printloss = false;

/*fmap for off momentum particle*/
strcpy(_FmapdpFlag_fmapdp_file,"fmapdp.out");
 _FmapdpFlag_nxpoint=31L, _FmapdpFlag_nepoint=21L, _FmapdpFlag_nturn=516L;
 _FmapdpFlag_xmax=0.025, _FmapdpFlag_emax=0.005, _FmapdpFlag_z=0.0;
 _FmapdpFlag_diffusion = true, _FmapdpFlag_printloss = false;
 
/* tune shift with amplitude*/
 strcpy(_AmplitudeTuneShift_nudx_file,"nudx.out");
 strcpy(_AmplitudeTuneShift_nudz_file,"nudz.out");
 _AmplitudeTuneShift_nxpoint=31L;  _AmplitudeTuneShift_nypoint=21L;
 _AmplitudeTuneShift_nturn=516L;   _AmplitudeTuneShift_xmax=0.025;
 _AmplitudeTuneShift_ymax=0.005,   _AmplitudeTuneShift_delta=0.0;

/* tune shift with energy*/
strcpy(_EnergyTuneShift_nudp_file,"nudp.out");
 _EnergyTuneShift_npoint=31L;  _EnergyTuneShift_nturn=516L;
 _EnergyTuneShift_deltamax=0.06;

/* random rotation coupling error*/
 err_seed=0L;  err_rms=0.0;  

/* momentum acceptance */
 strcpy(_MomentumAccFlag_momacc_file,"momentumacceptance.out");//default file name
 strcpy(_MomentumAccFlag_TrackDim,"6D"); //default track dimension
 _MomentumAccFlag_istart=1L, _MomentumAccFlag_istop=108L,
 _MomentumAccFlag_nstepn=100L, _MomentumAccFlag_nstepp=100L;
 _MomentumAccFlag_deltaminn=-0.01, _MomentumAccFlag_deltamaxn=-0.05;
 _MomentumAccFlag_deltaminp=0.01, _MomentumAccFlag_deltamaxp=0.05;
 _MomentumAccFlag_nturn = 1026;
 _MomentumAccFlag_zmax = 0.0003; //[m]

/* Phase space */
 strcpy(_Phase_phase_file,"phase.out");  //default phase file
 strcpy( _Phase_Dim,"4D"); //default phase dimension
 _Phase_X=0.0, _Phase_Px=0.0, _Phase_Y=0.0, _Phase_Py=0.0,
 _Phase_delta=0.0, _Phase_ctau=0.0;
 _Phase_nturn=512L;
 _Phase_Damping = false;
  
/* fit tunes for full quadrupole*/
 targetnux = 0.0, targetnuz = 0.0;

/* fit chromaticities*/
 targetksix = 0.0, targetksiz = 0.0;

};  
 
 };

/***** file names************/
extern char    lat_file[max_str];

//files with multipole errors; for soleil lattice
 extern char fic_hcorr[max_str],fic_vcorr[max_str], fic_skew[max_str];
 extern char multipole_file[max_str];
 //file of source of coupling; for soleil lattice
 extern char virtualskewquad_file[max_str];
 
 extern char track_file[max_str];
 extern char twiss_file[max_str];
 extern char cod_file[max_str];
 extern char girder_file[max_str];
  
 //files with the status of hcorr/vcorr status, to choose which correctors are used for orbit correction
 extern char    hcorr_file[max_str], vcorr_file[max_str];
// extern char    fe_file[max_str]; //the same as multipole_file[max_str]?????

//COD correction
 extern int nwh, nwv; 

// ID correction 
extern char   IDCq_name[max_str][11];


extern char hcorr_name[max_str], vcorr_name[max_str];
extern char skew_quad_name[max_str], bpm_name[max_str];
extern char gs_name[max_str], ge_name[max_str];

// function
 void read_script(const char *param_file_name, bool rd_lat,long& CommNo, UserCommand UserCommandFlag[]);
